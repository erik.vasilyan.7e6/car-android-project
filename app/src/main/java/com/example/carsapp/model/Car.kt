package com.example.carsapp.model

data class Car(
    val id: String,
    var name: String,
    val brand: String,
    var price: Int,
    var color: String,
    var fuel: String,
    var power: Int,
    val image: String,
    var gear: String,
    val seller: String
    )