package com.example.carsapp.model

data class User(val username: String, val password: String)