package com.example.carsapp.model

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.carsapp.R
import com.example.carsapp.databinding.ItemCarBinding
import com.example.carsapp.view.listeners.OnClickListener
import com.example.carsapp.view.repositories.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CarAdapter(private var cars: List<Car>, private val listener: OnClickListener):
    RecyclerView.Adapter<CarAdapter.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var repository: Repository

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemCarBinding.bind(view)
        fun setListener(car: Car){
            binding.root.setOnClickListener {
                listener.onClick(car)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        repository = Repository("admin", "admin")
        val view = LayoutInflater.from(context).inflate(R.layout.item_car, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cars.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val car = cars[position]

        with(holder) {
            setListener(car)
            binding.carNameTv.text = car.name
            binding.pricePortada.text = "${car.price} €"
            binding.fuelPortada.text = car.fuel
            binding.gearPortada.text = car.gear

            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getImage(car.id)
                withContext(Dispatchers.Main) {
                    if(response.isSuccessful && response.body() != null){
                        val carImage = response.body()!!.bytes()
                        Glide.with(context)
                            .load(carImage)
                            .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(25)))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(binding.carImageIv)
                    }
                    else {
                        Log.e("Error :", response.message())
                    }
                }
            }
        }
    }
}