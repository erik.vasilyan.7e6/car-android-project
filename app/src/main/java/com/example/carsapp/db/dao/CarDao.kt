package com.example.carsapp.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.carsapp.db.entities.CarEntity

@Dao
interface CarDao {
    @Query("SELECT * FROM cars WHERE user = :username")
    fun getAllFavoriteCars(username: String): MutableList<CarEntity>

    @Query("SELECT * FROM cars WHERE id = :id")
    fun getFavoriteCarById(id: String): MutableList<CarEntity>

    @Insert
    fun addCar(carEntity: CarEntity)

    @Delete
    fun deleteCar(carEntity: CarEntity)
}