package com.example.carsapp.db.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.carsapp.db.dao.CarDao
import com.example.carsapp.db.entities.CarEntity


@Database(entities = [CarEntity::class], version = 1)
abstract class CarDatabase: RoomDatabase() {
    abstract fun carDao(): CarDao
}