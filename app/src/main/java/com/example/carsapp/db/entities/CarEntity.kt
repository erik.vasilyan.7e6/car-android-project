package com.example.carsapp.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cars")
data class CarEntity(
    @PrimaryKey var id: String,
    val name: String,
    val brand: String,
    val price: Int,
    val color: String,
    val fuel: String,
    val power: Int,
    val image: String,
    val user: String,
    val gear: String
)