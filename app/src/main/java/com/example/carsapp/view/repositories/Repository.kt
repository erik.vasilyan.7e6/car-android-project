package com.example.carsapp.view.repositories

import com.example.carsapp.model.Car
import com.example.carsapp.model.User
import com.example.carsapp.view.interfaces.ApiInterface
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Header
import java.io.File

class Repository(val username: String, val password: String) {

    val apiInterface = ApiInterface.create(username, password)

    suspend fun getCars() = apiInterface.getCars()
    suspend fun getSellCars() = apiInterface.getSellCars()
    suspend fun deleteCar(id: String) = apiInterface.deleteCar(id)
    suspend fun getImage(imageId: String) = apiInterface.getImage(imageId)
    suspend fun putCar(car: Car) = apiInterface.putCar(car)

    suspend fun addCarToSell(id: RequestBody, name: RequestBody, brand: RequestBody, price: RequestBody, color: RequestBody, fuel: RequestBody, power: RequestBody, gear: RequestBody, image: MultipartBody.Part) = apiInterface.addCarToSell(id, name, brand, price, color, fuel ,power, gear, image)
    suspend fun register(user: User) = apiInterface.register(user)
    suspend fun login(user: User) = apiInterface.login(user)
}