package com.example.carsapp.view.fragments

import android.annotation.SuppressLint
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.carsapp.R
import com.example.carsapp.databinding.FragmentSingupBinding
import com.example.carsapp.model.User
import com.example.carsapp.view.activities.MainActivity
import com.example.carsapp.view.repositories.Repository
import com.example.carsapp.viewmodel.CarViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException

class SingupFragment : Fragment() {

    private lateinit var binding: FragmentSingupBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSingupBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity

        mainActivity.setBottomNavigationVisible(false)

        binding.btnGuardarDatos.setOnClickListener {
            val email = binding.edtEmailUser.text.toString()
            val password = binding.edtPasswordUser.text.toString()
            val passwordConfirm = binding.edtPasswordUserConfirm.text.toString()

            if (email.isEmpty() && password.isEmpty() && passwordConfirm.isEmpty()) {
                binding.txtInputEmailUser.error = "User cannot be empty"
                binding.txtInputPasswordUser.error = "Password cannot be empty"
                binding.txtInputPasswordUserConfirm.error = "Confirm password cannot be empty"
                return@setOnClickListener
            }

            if (email.isEmpty()) {
                binding.txtInputEmailUser.error = "User cannot be empty"
                return@setOnClickListener
            } else {
                binding.txtInputEmailUser.error = null
            }

            if (password.isEmpty() ) {
                binding.txtInputPasswordUser.error = "Password cannot be empty"
                return@setOnClickListener
            } else if(password.length < 5) {
                binding.txtInputPasswordUser.error = "Password must be at least 5 characters long"
                return@setOnClickListener
            }else{
                binding.txtInputPasswordUser.error = null
            }

            if (passwordConfirm.isEmpty() ) {
                binding.txtInputPasswordUserConfirm.error = "Confirm password cannot be empty"
                return@setOnClickListener
            } else if (password != passwordConfirm) {
                binding.txtInputPasswordUserConfirm.error = "Passwords do not match"
                return@setOnClickListener
            } else {
                binding.txtInputPasswordUserConfirm.error = null
            }

            createUser(User(binding.edtEmailUser.text.toString(), binding.edtPasswordUser.text.toString()))
        }

        binding.toolbar.setOnClickListener{
            findNavController().navigate(R.id.action_signupFragment_to_loginFragment)
        }

    }

    @SuppressLint("InflateParams")
    private fun createUser(user: User) {
        try {
            CoroutineScope(Dispatchers.IO).launch {
                val carViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]
                carViewModel.currentUser = user
                carViewModel.repository = Repository(user.username, user.password)
                val repository = Repository(user.username, user.password)
                val response = repository.register(user)
                withContext(Dispatchers.Main) {
                    if(response.isSuccessful){
                        val inflater = LayoutInflater.from(context)
                        val layout = inflater.inflate(R.layout.custom_toast_ok, null)
                        val toast = Toast(context)
                        toast.duration = Toast.LENGTH_SHORT
                        toast.view = layout
                        toast.show()
                        findNavController().navigate(R.id.action_signupFragment_to_carListFragment)
                    }
                    else{
                        val inflater = LayoutInflater.from(context)
                        val layout = inflater.inflate(R.layout.custom_toast_error, null)
                        val toast = Toast(context)
                        toast.duration = Toast.LENGTH_SHORT
                        toast.view = layout
                        toast.show()
                    }
                }
            }
        } catch (e: SocketTimeoutException) {
            Log.d(ContentValues.TAG, "SERVER NOT CONNECTED!")
        }
    }
}