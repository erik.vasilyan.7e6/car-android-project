package com.example.carsapp.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.carsapp.R

class LaunchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }
}