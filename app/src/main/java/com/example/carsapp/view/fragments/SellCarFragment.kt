package com.example.carsapp.view.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.carsapp.R
import com.example.carsapp.databinding.FragmentSellCarBinding
import com.example.carsapp.model.Car
import com.example.carsapp.view.activities.MainActivity
import com.example.carsapp.viewmodel.CarViewModel
import java.io.File

class SellCarFragment : Fragment() {

    private lateinit var binding: FragmentSellCarBinding
    private lateinit var imageUri: Uri
    private lateinit var carViewModel : CarViewModel

    private var resultLauncherImage = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                imageUri = data.data!!
                carViewModel.imageUri = imageUri
                Glide.with(this)
                    .load(imageUri)
                    .into(binding.imgVacia)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSellCarBinding.inflate(layoutInflater)
        carViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        val carViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]

        var lastId = "1"
        if (carViewModel.cars.value!!.isNotEmpty()) {
            lastId = (carViewModel.cars.value!!.last().id.toInt() + 1).toString()
        }

        binding.bottomBuy.setOnClickListener {
            if (!::imageUri.isInitialized || binding.nameUserEdittext.text.toString() == "" || binding.colorsSpinner.selectedItem.toString() == "Color" || binding.brandSpinner.selectedItem.toString() == "Brand" || binding.fuelSpinner.selectedItem.toString() == "Fuel" || binding.gearSpinner.selectedItem.toString() == "Gear" || binding.priceUserEdittext.text.toString() == "" || binding.powerUserEdittext.text.toString() == "") {
                showToast()
                return@setOnClickListener
            }

            val carToSell = Car(lastId, binding.nameUserEdittext.text.toString(), binding.brandSpinner.selectedItem.toString(), binding.priceUserEdittext.text.toString().toInt(), binding.colorsSpinner.selectedItem.toString(), binding.fuelSpinner.selectedItem.toString(), binding.powerUserEdittext.text.toString().toInt(), lastId, binding.gearSpinner.selectedItem.toString(), carViewModel.currentUser.username)
            carViewModel.addCarToSell(carToSell, getFileFromUri(requireContext(), carViewModel.imageUri)!!)
            findNavController().navigate(R.id.action_sellCarFragment_to_myCarsFragment)
        }

        binding.imgVacia.setOnClickListener {
            selectImage()
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncherImage.launch(intent)
    }

    private fun getFileFromUri(context: Context, uri: Uri): File? {
        val inputStream = context.contentResolver.openInputStream(uri) ?: return null
        val fileName = uri.lastPathSegment ?: "file"
        val directory = context.getExternalFilesDir(null)
        val file = File(directory, fileName)
        inputStream.use { input ->
            file.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        return if (file.exists()) file else null
    }

    @SuppressLint("InflateParams")
    private fun showToast() {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.custom_toast_error_add_car, null)
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }
}