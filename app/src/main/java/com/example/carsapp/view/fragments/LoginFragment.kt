package com.example.carsapp.view.fragments

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.carsapp.R
import com.example.carsapp.databinding.FragmentLoginBinding
import com.example.carsapp.model.User
import com.example.carsapp.view.activities.MainActivity
import com.example.carsapp.view.repositories.Repository
import com.example.carsapp.viewmodel.CarViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity

        mainActivity.setBottomNavigationVisible(false)

        binding.txtNuevoUsuario.setOnClickListener{
            findNavController().navigate(R.id.action_loginFragment_to_signupFragment)

        }

        binding.btnIniciarSesion.setOnClickListener {
            // Get the email and password from the input fields
            val email = binding.edtMail.text.toString()
            val password = binding.edtPassword.text.toString()

            // Check if both email and password are empty
            if (email.isEmpty() && password.isEmpty()) {
                binding.txtInputUsuario.error = "Email cannot be empty"
                binding.txtInputPassword.error = "Password cannot be empty"
                return@setOnClickListener
            }

            // Check if the email is empty
            if (email.isEmpty()) {
                binding.txtInputUsuario.error = "Email cannot be empty"
                return@setOnClickListener
            } else {
                binding.txtInputUsuario.error = null // Clear the error message
            }

            // Check if the password is empty or too short
            if (password.isEmpty() ) {
                binding.txtInputPassword.error = "Password cannot be empty"
                return@setOnClickListener
            } else if(password.length < 5) {
                binding.txtInputPassword.error = "Password must be at least 6 characters long"
                return@setOnClickListener
            }else{
                binding.txtInputPassword.error = null // Clear the error message
            }

            // Call the loginUser function if all checks pass
            loginUser(User(email, password))
        }
    }

    @SuppressLint("InflateParams")
    private fun loginUser(user: User) {
        try {
            CoroutineScope(Dispatchers.IO).launch {
                val carViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]
                carViewModel.currentUser = user
                carViewModel.repository = Repository(user.username, user.password)
                val repository = Repository(user.username, user.password)
                val response = repository.login(user)
                withContext(Dispatchers.Main){
                    if (response.isSuccessful){
                        val inflater = LayoutInflater.from(context)
                        val layout = inflater.inflate(R.layout.custom_toast_ok, null)
                        val toast = Toast(context)
                        toast.duration = Toast.LENGTH_SHORT
                        toast.view = layout
                        toast.show()
                        findNavController().navigate(R.id.action_loginFragment_to_carListFragment)
                    }else{
                        val inflater = LayoutInflater.from(context)
                        val layout = inflater.inflate(R.layout.custom_toast_error, null)
                        val toast = Toast(context)
                        toast.duration = Toast.LENGTH_SHORT
                        toast.view = layout
                        toast.show()
                    }
                }
            }
        } catch (e: SocketTimeoutException) {
            Log.d(TAG, "SERVER NOT CONNECTED!")
        }
    }
}

