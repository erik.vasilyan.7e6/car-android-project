package com.example.carsapp.view.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SearchView
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.carsapp.R
import com.example.carsapp.databinding.FragmentFavoritesBinding
import com.example.carsapp.model.Car
import com.example.carsapp.model.CarAdapter
import com.example.carsapp.view.activities.MainActivity
import com.example.carsapp.view.listeners.OnClickListener
import com.example.carsapp.viewmodel.CarViewModel

class FavoritesFragment : Fragment(), OnClickListener {

    private lateinit var binding: FragmentFavoritesBinding
    private lateinit var carAdapter: CarAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private var areCarsStored = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFavoritesBinding.inflate(layoutInflater)

        carAdapter = CarAdapter(mutableListOf(), this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = carAdapter
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)

        val carViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]

        carViewModel.fetchFavoriteCars()

        carViewModel.currentFragment = 1

        carViewModel.favoriteCars.observe(viewLifecycleOwner) {
            areCarsStored = true
            setupRecyclerView(it!!)
        }

        binding.searchSv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.toString() != "") {
                    carViewModel.findCarsByName(newText.toString())
                    setupRecyclerView(carViewModel.filteredCarsList)
                }
                else {
                    if (areCarsStored) setupRecyclerView(carViewModel.favoriteCars.value!!)
                }
                return false
            }
        })

        binding.filterButton.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_custom, null)
            val builder = AlertDialog.Builder(context)
            builder.setTitle("FILTER CARS BY...")
            builder.setView(dialogView)
            builder.setPositiveButton("OK") { _, _ ->
                carViewModel.addCarsToFilteredList()

                val minPrice = dialogView.findViewById<EditText>(R.id.min_price_et).text.toString()
                val maxPrice = dialogView.findViewById<EditText>(R.id.max_price_et).text.toString()
                val color = dialogView.findViewById<Spinner>(R.id.a_colors_spinner).selectedItem.toString()
                val fuel = dialogView.findViewById<Spinner>(R.id.fuel_spinner).selectedItem.toString()
                val brand = dialogView.findViewById<Spinner>(R.id.brand_spinner).selectedItem.toString()
                val gear = dialogView.findViewById<Spinner>(R.id.gear_spinner).selectedItem.toString()

                if (carViewModel.favoriteCars.value!!.isNotEmpty()) {
                    if (minPrice != "" && maxPrice != "") {
                        carViewModel.filterCarsByPrice(minPrice.toInt(), maxPrice.toInt())
                    }
                    if (color != "- Any -") {
                        carViewModel.filterCarsByColor(color)
                    }
                    if (fuel != "- Any -") {
                        carViewModel.filterCarsByFuel(fuel)
                    }
                    if (brand != "- Any -") {
                        carViewModel.filterCarsByBrand(brand)
                    }
                    if (gear != "- Any -") {
                        carViewModel.filterCarsByGear(gear)
                    }

                    if (minPrice == "" && maxPrice == "" && color == "- Any -" && fuel == "- Any -" && brand == "- Any -" && gear == "- Any -") {
                        setupRecyclerView(carViewModel.favoriteCars.value!!)
                    }
                    else setupRecyclerView(carViewModel.filteredCarsList)
                }
            }
            builder.setNegativeButton("Cancel") { dialogInterface, _ ->
                dialogInterface.cancel()
            }
            builder.show()
        }
    }

    private fun setupRecyclerView(cars: List<Car>) {
        carAdapter = CarAdapter(cars, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = carAdapter
        }
    }

    override fun onClick(car: Car) {
        val carViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]
        carViewModel.car.postValue(car)
        findNavController().navigate(R.id.action_favoritesFragment_to_detailFragment)
    }
}