package com.example.carsapp.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.carsapp.R
import com.example.carsapp.databinding.FragmentBuyBinding
import com.example.carsapp.view.activities.MainActivity

class BuyFragment : Fragment() {

    private lateinit var binding: FragmentBuyBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentBuyBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("InflateParams")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity

        mainActivity.setBottomNavigationVisible(false)

        binding.toolbar.setOnClickListener{
            findNavController().navigate(R.id.action_buyFragment_to_detailFragment)
        }

        binding.contact.setOnClickListener{

            val email = binding.edtEmail.text.toString()
            val name = binding.edtName.text.toString()
            val phone = binding.edtPhone.text.toString()

            if (email.isEmpty() && name.isEmpty() && phone.isEmpty()) {
                binding.txtInputEmail.error = "Email cannot be empty"
                binding.txtInputName.error = "Name cannot be empty"
                binding.txtInputPhone.error = "Phone cannot be empty"
                return@setOnClickListener
            }

            val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

            if (email.isEmpty()) {
                binding.txtInputEmail.error = "Email cannot be empty"
                return@setOnClickListener
            } else if (!email.matches(emailPattern.toRegex())) {
                binding.txtInputEmail.error = "Invalid email format"
                return@setOnClickListener
            } else {
                binding.txtInputEmail.error = null
            }

            if (name.isEmpty() ) {
                binding.txtInputName.error = "Name cannot be empty"
                return@setOnClickListener
            } else {
                binding.txtInputName.error = null
            }

            if (phone.isEmpty() ) {
                binding.txtInputPhone.error = "Name cannot be empty"
                return@setOnClickListener
            } else {
                binding.txtInputPhone.error = null
            }

            val inflater = LayoutInflater.from(context)
            val layout = inflater.inflate(R.layout.custom_toast_reserved, null)
            val toast = Toast(context)
            toast.duration = Toast.LENGTH_SHORT
            toast.view = layout
            toast.show()

            findNavController().navigate(R.id.action_buyFragment_to_carListFragment)
        }
    }
}