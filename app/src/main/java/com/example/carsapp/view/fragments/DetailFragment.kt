package com.example.carsapp.view.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.carsapp.R
import com.example.carsapp.databinding.FragmentDetailBinding
import com.example.carsapp.model.Car
import com.example.carsapp.view.activities.MainActivity
import com.example.carsapp.viewmodel.CarViewModel

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    lateinit var car: Car

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n", "CutPasteId")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity

        mainActivity.setBottomNavigationVisible(false)

        val carsViewModel = ViewModelProvider(requireActivity())[CarViewModel::class.java]

        carsViewModel.isCarInFavorites()

        carsViewModel.car.observe(viewLifecycleOwner) {
            car = it

            Glide.with(requireContext())
                .load(car.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imgCar)
            binding.titleCar.text = car.name
            binding.price.text = "${car.price} €"
            binding.power.text = "${car.power} CV"
            binding.color.text = car.color
            binding.fuel.text = car.fuel
            binding.gear.text = car.gear
        }

        carsViewModel.isTheDetailCarInFavorites.observe(viewLifecycleOwner) {
            if (it) {
                binding.bottomGuardar.visibility = View.GONE
                binding.bottomEliminar.visibility = View.VISIBLE
            }
            else {
                binding.bottomGuardar.visibility = View.VISIBLE
                binding.bottomEliminar.visibility = View.GONE
            }

            when (carsViewModel.currentFragment) {
                0 -> {
                    binding.bottomCompartir.visibility = View.VISIBLE
                    binding.bottomContact.visibility = View.VISIBLE
                    binding.bottomDeleteSellCar.visibility = View.GONE
                    binding.bottomModificar.visibility = View.GONE
                }
                1 -> {
                    binding.bottomCompartir.visibility = View.VISIBLE
                    binding.bottomContact.visibility = View.VISIBLE
                    binding.bottomDeleteSellCar.visibility = View.GONE
                    binding.bottomModificar.visibility = View.GONE
                }
                2 -> {
                    binding.bottomGuardar.visibility = View.GONE
                    binding.bottomCompartir.visibility = View.VISIBLE
                    binding.bottomContact.visibility = View.GONE
                    binding.bottomEliminar.visibility = View.GONE
                    binding.bottomDeleteSellCar.visibility = View.VISIBLE
                    binding.bottomModificar.visibility = View.VISIBLE
                }
            }
        }

        binding.flechaAtras.setOnClickListener {
            when (carsViewModel.currentFragment) {
                0 -> {
                    findNavController().navigate(R.id.action_detailFragment_to_carListFragment)
                }
                1 -> {
                    findNavController().navigate(R.id.action_detailFragment_to_favoritesFragment)
                }
                else -> {
                    findNavController().navigate(R.id.action_detailFragment_to_myCarsFragment)
                }
            }
        }

        binding.bottomGuardar.setOnClickListener {
            carsViewModel.addCarToFavorites()
            binding.bottomGuardar.visibility = View.GONE
            binding.bottomEliminar.visibility = View.VISIBLE
        }

        binding.bottomEliminar.setOnClickListener {
            carsViewModel.deleteCarFromFavorites()
            binding.bottomGuardar.visibility = View.VISIBLE
            binding.bottomEliminar.visibility = View.GONE
        }

        binding.bottomContact.setOnClickListener {
            findNavController().navigate(R.id.action_detailFragment_to_buyFragment)
        }

        binding.bottomCompartir.setOnClickListener {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, "I'm selling ${car.name}, the price is ${car.price}, contact me if you are interested")
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }

        binding.bottomDeleteSellCar.setOnClickListener {
            carsViewModel.deleteCarFromSells()
            findNavController().navigate(R.id.action_detailFragment_to_myCarsFragment)
        }

        binding.bottomModificar.setOnClickListener {

            binding.editModifyTitle.visibility = View.VISIBLE
            binding.editModifyPrice.visibility = View.VISIBLE
            binding.editModifyPower.visibility = View.VISIBLE
            binding.editModifyColor.visibility = View.VISIBLE
            binding.editModifyFuel.visibility = View.VISIBLE
            binding.editModifyGear.visibility = View.VISIBLE

        }

        binding.bottomModificar.setOnClickListener {
            if (binding.bottomModificar.text == "MODIFY") {
                binding.bottomModificar.setIconResource(R.drawable.ic_editoff)
                binding.editModifyTitle.visibility = View.VISIBLE
                binding.editModifyPrice.visibility = View.VISIBLE
                binding.editModifyPower.visibility = View.VISIBLE
                binding.editModifyColor.visibility = View.VISIBLE
                binding.editModifyFuel.visibility = View.VISIBLE
                binding.editModifyGear.visibility = View.VISIBLE
                binding.bottomModificar.text = "MAINTAIN"
            }
            else {
                binding.bottomModificar.setIconResource(R.drawable.ic_edit)
                binding.editModifyTitle.visibility = View.GONE
                binding.editModifyPrice.visibility = View.GONE
                binding.editModifyPower.visibility = View.GONE
                binding.editModifyColor.visibility = View.GONE
                binding.editModifyFuel.visibility = View.GONE
                binding.editModifyGear.visibility = View.GONE
                binding.bottomModificar.text = "MODIFY"
            }
        }

            binding.editModifyTitle.setOnClickListener {
                val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit, null)
                val textView = dialogView.findViewById<TextView>(R.id.title)
                textView.text = "Name:" // Change the text to "New Text"

                val builder = AlertDialog.Builder(context)
                builder.setTitle("EDIT CAR")
                builder.setView(dialogView)
                builder.setPositiveButton("ACCEPT") { _, _ ->
                    val title = dialogView.findViewById<EditText>(R.id.detail_dialog_et).text.toString()
                    car.name = title
                    carsViewModel.modifyCar(car)
                    binding.titleCar.text = title
                }
                builder.show()
            }

        binding.editModifyPrice.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit, null)
            val textView = dialogView.findViewById<TextView>(R.id.title)
            textView.text = "Price:"
            val editText = dialogView.findViewById<TextView>(R.id.detail_dialog_et)
            editText.inputType = InputType.TYPE_CLASS_NUMBER
            val builder = AlertDialog.Builder(context)
            builder.setTitle("EDIT CAR")
            builder.setView(dialogView)
            builder.setPositiveButton("ACCEPT") { _, _ ->
                val price = dialogView.findViewById<EditText>(R.id.detail_dialog_et).text.toString().toInt()
                car.price = price
                carsViewModel.modifyCar(car)
                binding.price.text = "$price €"
            }
            builder.show()
        }
        binding.editModifyPower.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit, null)
            val textView = dialogView.findViewById<TextView>(R.id.title)
            textView.text = "Power:"
            val editText = dialogView.findViewById<TextView>(R.id.detail_dialog_et)
            editText.inputType = InputType.TYPE_CLASS_NUMBER
            val builder = AlertDialog.Builder(context)
            builder.setTitle("EDIT CAR")
            builder.setView(dialogView)
            builder.setPositiveButton("ACCEPT") { _, _ ->
                val power = dialogView.findViewById<EditText>(R.id.detail_dialog_et).text.toString().toInt()
                car.power = power
                carsViewModel.modifyCar(car)
                binding.power.text = "$power CV"
            }
            builder.show()
        }

        binding.editModifyColor.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit_color, null)
            val textView = dialogView.findViewById<TextView>(R.id.title)
            textView.text = "Color"
            val builder = AlertDialog.Builder(context)
            builder.setTitle("EDIT CAR")
            builder.setView(dialogView)
            builder.setPositiveButton("ACCEPT") { _, _ ->
                val color = dialogView.findViewById<Spinner>(R.id.detail_dialog_et).selectedItem.toString()
                car.color = color
                carsViewModel.modifyCar(car)
                binding.color.text = color
            }
            builder.show()
        }

        binding.editModifyFuel.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit_fuel, null)
            val textView = dialogView.findViewById<TextView>(R.id.title)
            textView.text = "Fuel" // Change the text to "New Text"
            val builder = AlertDialog.Builder(context)
            builder.setTitle("EDIT CAR")
            builder.setView(dialogView)
            builder.setPositiveButton("ACCEPT") { _, _ ->
                val fuel = dialogView.findViewById<Spinner>(R.id.detail_dialog_et).selectedItem.toString()
                car.fuel = fuel
                carsViewModel.modifyCar(car)
                binding.fuel.text = fuel
            }
            builder.show()
        }
        binding.editModifyGear.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit_gear, null)
            val textView = dialogView.findViewById<TextView>(R.id.title)
            textView.text = "Gear" // Change the text to "New Text"
            val builder = AlertDialog.Builder(context)
            builder.setTitle("EDIT CAR")
            builder.setView(dialogView)
            builder.setPositiveButton("ACCEPT") { _, _ ->
                val gear = dialogView.findViewById<Spinner>(R.id.detail_dialog_et).selectedItem.toString()
                car.gear = gear
                carsViewModel.modifyCar(car)
                binding.gear.text = gear
            }
            builder.show()
        }
    }
}