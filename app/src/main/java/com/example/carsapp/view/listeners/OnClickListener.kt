package com.example.carsapp.view.listeners

import com.example.carsapp.model.Car

interface OnClickListener {
    fun onClick(car: Car)
}