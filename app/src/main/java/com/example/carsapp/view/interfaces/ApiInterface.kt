package com.example.carsapp.view.interfaces

import com.burgstaller.okhttp.digest.Credentials
import com.burgstaller.okhttp.digest.DigestAuthenticator
import com.example.carsapp.model.Car
import com.example.carsapp.model.User
import com.google.gson.GsonBuilder
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {
    @GET("cars")
    suspend fun getCars(): Response<List<Car>>

    @GET("cars/sells")
    suspend fun getSellCars(): Response<List<Car>>

    @Multipart
    @POST("cars")
    suspend fun addCarToSell(
        @Part ("id") id: RequestBody,
        @Part ("name") name: RequestBody,
        @Part ("brand") brand: RequestBody,
        @Part ("price") price: RequestBody,
        @Part ("color") color: RequestBody,
        @Part ("fuel") fuel: RequestBody,
        @Part ("power") power: RequestBody,
        @Part ("gear") gear: RequestBody,
        @Part image: MultipartBody.Part
    ): Response<ResponseBody>

    @POST("register")
    suspend fun register(@Body user: User): Response<ResponseBody>

    @POST("login")
    suspend fun login(@Body user: User): Response<ResponseBody>

    @DELETE("cars/{id}")
    suspend fun deleteCar(@Path("id") id: String): Response<ResponseBody>

    @GET("uploads/{imageId}")
    suspend fun getImage(@Path("imageId") imageId: String): Response<ResponseBody>

    @PUT("cars")
    suspend fun putCar(@Body car: Car): Response<ResponseBody>



    companion object {

        private const val BASE_URL = "http://192.168.56.1:8080/" // IP DEL ORDENADOR, EL PUERTO ES 8080

        fun create(username: String, password: String): ApiInterface {
            val digestAuthenticator = DigestAuthenticator(Credentials(username, password))

            val client = OkHttpClient.Builder()
                .authenticator(digestAuthenticator)
                .build()

            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}