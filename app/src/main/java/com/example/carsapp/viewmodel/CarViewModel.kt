package com.example.carsapp.viewmodel

import android.content.ContentValues.TAG
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.carsapp.db.applications.CarApplication
import com.example.carsapp.db.entities.CarEntity
import com.example.carsapp.model.Car
import com.example.carsapp.model.User
import com.example.carsapp.view.repositories.Repository
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.util.concurrent.CopyOnWriteArrayList

class CarViewModel: ViewModel() {

    val cars = MutableLiveData<List<Car>>()
    var car = MutableLiveData<Car>()
    var filteredCarsList = CopyOnWriteArrayList<Car>()
    val favoriteCars = MutableLiveData<List<Car>?>()
    val sellingCars = MutableLiveData<List<Car>?>()
    var currentUser = User("admin", "admin")
    lateinit var repository: Repository
    val isTheDetailCarInFavorites = MutableLiveData<Boolean>()
    var currentFragment = 0
    lateinit var imageUri: Uri

    fun fetchCars() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCars()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    cars.postValue(response.body())
                }
                else {
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun fetchFavoriteCars() {
        CoroutineScope(Dispatchers.Main).launch {
            favoriteCars.value = withContext(Dispatchers.IO) {
                CarApplication.database.carDao().getAllFavoriteCars(currentUser.username).map { carEntity ->
                    convertCarEntityToObject(carEntity)
                }.toCollection(mutableListOf()).toList()
            }
        }
    }

    fun fetchSellingCars() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getSellCars()
            withContext(Dispatchers.Main) {
                if(response.isSuccessful && response.body() != null){
                    sellingCars.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun addCarToSell(carToSell: Car, image: File) {
        CoroutineScope(Dispatchers.IO).launch {
            val imagePart = MultipartBody.Part.createFormData("jpeg", image.name, image.asRequestBody("image/*".toMediaType()))
            val response = repository.addCarToSell(
                carToSell.id.toRequestBody("text/plain".toMediaType()),
                carToSell.name.toRequestBody("text/plain".toMediaType()),
                carToSell.brand.toRequestBody("text/plain".toMediaType()),
                carToSell.price.toString().toRequestBody("text/plain".toMediaType()),
                carToSell.color.toRequestBody("text/plain".toMediaType()),
                carToSell.fuel.toRequestBody("text/plain".toMediaType()),
                carToSell.power.toString().toRequestBody("text/plain".toMediaType()),
                carToSell.gear.toRequestBody("text/plain".toMediaType()),
                imagePart
            )
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Log.d(TAG, "CAR POSTED SUCCESSFULLY!")
                }
                else {
                    Log.d(TAG, "ERROR, CAR NOT POSTED!")
                }
            }
        }
    }

    fun deleteCarFromSells() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.deleteCar(car.value!!.id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Log.d(TAG, "CAR DELETED SUCCESSFULLY!")
                }
                else {
                    Log.d(TAG, "ERROR, CAR NOT DELETED!")
                }
            }
        }
    }

        fun modifyCar(carToModify: Car) {
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.putCar(carToModify)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        Log.d(TAG, "CAR MODIFIED SUCCESSFULLY!")
                    } else {
                        Log.d(TAG, "ERROR, CAR NOT MODIFIED!")
                    }
                }
            }
        }



    fun addCarToFavorites() {
        val carToAddInFavorites = convertCarObjectToEntity(car.value!!)

        CoroutineScope(Dispatchers.IO).launch {
            CarApplication.database.carDao().addCar(carToAddInFavorites)
        }
    }

    fun deleteCarFromFavorites() {
        val carToDeleteFromFavorites = convertCarObjectToEntity(car.value!!)

        CoroutineScope(Dispatchers.IO).launch {
            CarApplication.database.carDao().deleteCar(carToDeleteFromFavorites)
        }
    }

    fun isCarInFavorites() {
        var isFavorite: Boolean
        runBlocking {
            withContext(Dispatchers.IO) {
                val carInDatabase = CarApplication.database.carDao().getFavoriteCarById(car.value!!.id)
                isFavorite = carInDatabase.size != 0
            }
        }
        isTheDetailCarInFavorites.postValue(isFavorite)
    }

    fun findCarsByName(name: String) {
        filteredCarsList = CopyOnWriteArrayList<Car>()

        when (currentFragment) {
            0 -> {
                for (car in cars.value!!) {
                    if (car.name.lowercase().contains(name.lowercase())) {
                        filteredCarsList.add(car)
                    }
                }
            }
            1 -> {
                for (car in favoriteCars.value!!) {
                    if (car.name.lowercase().contains(name.lowercase())) {
                        filteredCarsList.add(car)
                    }
                }
            }
            2 -> {
                for (car in sellingCars.value!!) {
                    if (car.name.lowercase().contains(name.lowercase())) {
                        filteredCarsList.add(car)
                    }
                }
            }
        }
    }

    fun addCarsToFilteredList() {
        filteredCarsList = CopyOnWriteArrayList<Car>()

        for (car in cars.value!!) {
            filteredCarsList.add(car)
        }
    }

    fun filterCarsByColor(color: String) {
        for (car in filteredCarsList) {
            if (car.color != color) {
                filteredCarsList.remove(car)
            }
        }
    }

    fun filterCarsByPrice(minPrice: Int, maxPrice: Int) {
        for (car in filteredCarsList) {
            if (car.price !in (minPrice..maxPrice)) {
                filteredCarsList.remove(car)
            }
        }
    }

    fun filterCarsByFuel(fuel: String) {
        for (car in filteredCarsList) {
            if (car.fuel != fuel) {
                filteredCarsList.remove(car)
            }
        }
    }

    fun filterCarsByBrand(brand: String) {
        for (car in filteredCarsList) {
            if (car.brand != brand) {
                filteredCarsList.remove(car)
            }
        }
    }

    fun filterCarsByGear(gear: String) {
        for (car in filteredCarsList) {
            if (car.gear != gear) {
                filteredCarsList.remove(car)
            }
        }
    }

    private fun convertCarEntityToObject(carEntity: CarEntity): Car {
        return Car(carEntity.id, carEntity.name, carEntity.brand, carEntity.price, carEntity.color, carEntity.fuel, carEntity.power, carEntity.image, carEntity.gear, "")
    }

    private fun convertCarObjectToEntity(car: Car): CarEntity {
        return CarEntity(car.id, car.name, car.brand, car.price, car.color, car.fuel, car.price, car.image, currentUser.username, car.gear)
    }
}